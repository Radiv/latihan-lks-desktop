﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LatihanH1
{
    public partial class MainForm : Form
    {
        Controls.DefaultForm defaultForm = new Controls.DefaultForm();
        Controls.EmployeeForm employeeForm = new Controls.EmployeeForm();
        Controls.PackageForm packageForm = new Controls.PackageForm();
        Controls.ServiceForm serviceForm = new Controls.ServiceForm();
        Controls.ViewTransaction ViewTransaction = new Controls.ViewTransaction();
        Controls.TransactionDeposit transactionDeposit = new Controls.TransactionDeposit();
        Controls.PrepaidForm prepaidForm = new Controls.PrepaidForm();

        public int id;
        public string nama;
        public MainForm(int id, string nama)
        {
            InitializeComponent();
            mainPanel.Controls.Clear();
            mainPanel.Controls.Add(defaultForm);
            defaultForm.Dock = DockStyle.Fill;
            timer1.Start();

            this.nama = nama;
            this.id = id;
            labelUsername.Text = "Welcome, "+ this.nama;
        }

        private void btnEmployee_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Clear();
            mainPanel.Controls.Add(employeeForm);
            employeeForm.Dock = DockStyle.Fill;
        }

        private void btnService_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Clear();
            mainPanel.Controls.Add(serviceForm);
            serviceForm.Dock = DockStyle.Fill;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(sidePanel.Visible == true)
            {
                sidePanel.Visible = false;
            } else
            {
                sidePanel.Visible = true;
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            this.Hide();
            form1.ShowDialog();
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            time.Text = DateTime.Now.ToString("G");
        }

        private void btnPackage_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Clear();
            mainPanel.Controls.Add(packageForm);
            packageForm.Dock = DockStyle.Fill;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Clear();
            mainPanel.Controls.Add(ViewTransaction);
            ViewTransaction.Dock = DockStyle.Fill;
        }

        private void btnTransaction_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Clear();
            mainPanel.Controls.Add(transactionDeposit);
            transactionDeposit.Dock = DockStyle.Fill;
        }

        private void btnPrepaid_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Clear();
            mainPanel.Controls.Add(prepaidForm);
            prepaidForm.Dock = DockStyle.Fill;
        }
    }
}
