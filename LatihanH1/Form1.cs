﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LatihanH1
{
    public partial class Form1 : Form
    {
        SqlConnection con = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=Laundry;Integrated Security=True");

        public Form1()
        {
            InitializeComponent();
        }
        
        private void btnLogin_Click(object sender, EventArgs e)
        {
            con.Open();
            String query = "SELECT * FROM dbo.Employee Where email = '"+textBox1.Text.Trim()+"' and password = '"+textBox2.Text.Trim()+"' ";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if(dt.Rows.Count == 1) 
            { 
                DataRow row = dt.Rows[0];
                MainForm mainForm = new MainForm(int.Parse(row["id"].ToString()), row["name"].ToString());
                this.Hide();
                mainForm.ShowDialog();
                this.Close();
            } else
            {
                MessageBox.Show("Please Try Again, Your Data is not Valid!");
            }
            con.Close();
        }
    }
}
