﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace LatihanH1.Controls
{
    public partial class ViewTransaction : UserControl
    {
        public ViewTransaction()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=Laundry;Integrated Security=True");

        private void ViewTransaction_Load(object sender, EventArgs e)
        {
            LoadHeader();
        }
        void LoadHeader()
        {
            con.Open();
            string query = "SELECT dbo.HeaderDeposit.Id AS Id, dbo.HeaderDeposit.IdCustomer AS [Id Customer], dbo.Customer.Name AS [Customer Name], " +
                "dbo.Employee.Name AS [Employee Name], dbo.HeaderDeposit.TransactionDatetime, dbo.HeaderDeposit.CompleteEstimationDatetime FROM dbo.HeaderDeposit " +
                "JOIN dbo.Customer ON dbo.Customer.Id = dbo.HeaderDeposit.IdCustomer " +
                "JOIN dbo.Employee ON dbo.Employee.Id = dbo.HeaderDeposit.IdEmployee";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridHeader.DataSource = dt;
            con.Close();

        }

        private void dataGridHeader_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridHeader.CurrentRow.Selected = true;

            int row = dataGridHeader.CurrentCell.RowIndex;

            con.Open();
            string query = "SELECT dbo.DetailDeposit.Id, dbo.Service.Name, dbo.DetailDeposit.IdPrepaidPackage, dbo.DetailDeposit.PriceUnit, " +
                "dbo.DetailDeposit.TotalUnit, dbo.DetailDeposit.CompletedDatetime FROM dbo.DetailDeposit " +
                "JOIN dbo.Service ON dbo.Service.Id = dbo.DetailDeposit.IdService " +
                "where IdDeposit = " + dataGridHeader.Rows[row].Cells[0].Value;
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridDetail.DataSource = dt;
            dataGridDetail.Columns[0].Visible = false;
            con.Close();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            (dataGridHeader.DataSource as DataTable).DefaultView.RowFilter =
                //String.Format("[Customer Name] like '%" + textBox3.Text + "%' or [Employee Name] like '%" + textBox3.Text + "%' or TransactionDatetime = '" + textBox3.Text + "'");
                String.Format("[Customer Name] like '%" + textBox3.Text + "%' or [Employee Name] like '%" + textBox3.Text + "%'");
        }
    }
}
