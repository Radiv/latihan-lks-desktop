﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace LatihanH1.Controls
{
    public partial class EmployeeForm : UserControl
    {
        SqlConnection con = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=Laundry;Integrated Security=True");
        private int ID = 0;

        public EmployeeForm()
        {
            InitializeComponent();
        }
        
        private void tampilData(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.CurrentRow.Selected = true;

            int row = dataGridView1.CurrentCell.RowIndex;

            ID = (int)dataGridView1.Rows[row].Cells[0].Value;
            txtIdEmployee.Text = dataGridView1.Rows[row].Cells[0].Value.ToString();
            txtPassword.Text = dataGridView1.Rows[row].Cells[1].Value.ToString();
            txtName.Text = dataGridView1.Rows[row].Cells[2].Value.ToString();
            txtEmail.Text = dataGridView1.Rows[row].Cells[3].Value.ToString();
            txtAddress.Text = dataGridView1.Rows[row].Cells[4].Value.ToString();
            txtPhone.Text = dataGridView1.Rows[row].Cells[5].Value.ToString();
            dateTimePicker1.Text = dataGridView1.Rows[row].Cells[6].Value.ToString();
            comboBox1.SelectedValue = dataGridView1.Rows[row].Cells[7].Value;
            numericUpDown1.Value = (decimal) dataGridView1.Rows[row].Cells[8].Value;
            txtConfirmPassword.Text = dataGridView1.Rows[row].Cells[1].Value.ToString();
        }

        private void EmployeeForm_Load(object sender, EventArgs e)
        {
            Refresh();
            LoadJobTitle();
        }
        void LoadAllRecord()
        {
            con.Open();
            string query = "SELECT * FROM dbo.Employee";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["Password"].Visible = false;
            con.Close();
        }
        void LoadJobTitle()
        {
            con.Open();
            string query = "SELECT * FROM dbo.Job  ";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "Id";
            con.Close();
        }

        void Refresh()
        {
            dataGridView1.Enabled = true;
            ID = 0;

            txtIdEmployee.Clear();
            txtName.Clear();
            txtEmail.Clear();
            txtPhone.Clear();
            txtAddress.Clear();
            dateTimePicker1.Value = DateTime.Now;
            comboBox1.SelectedItem = null;
            numericUpDown1.Value = 0;
            txtPassword.Clear();
            txtConfirmPassword.Clear();

            txtIdEmployee.Enabled = false;
            txtName.Enabled = false;
            txtEmail.Enabled = false;
            txtPhone.Enabled = false;
            txtAddress.Enabled = false;
            dateTimePicker1.Enabled = false;
            comboBox1.Enabled = false;
            numericUpDown1.Enabled = false;
            txtPassword.Enabled = false;
            txtConfirmPassword.Enabled = false;

            btnInsert.Enabled = true;
            btnDelete.Enabled = true;
            btnUpdate.Enabled = true;

            btnSave.Enabled = false;
            btnCancel.Enabled = false;

            LoadAllRecord();
        }

        void actionClick()
        {
            dataGridView1.Enabled = false;

            //txtIdEmployee.Enabled = true;
            txtName.Enabled = true;
            txtEmail.Enabled = true;
            txtPhone.Enabled = true;
            txtAddress.Enabled = true;
            dateTimePicker1.Enabled = true;
            comboBox1.Enabled = true;
            numericUpDown1.Enabled = true;
            txtPassword.Enabled = true;
            txtConfirmPassword.Enabled = true;

            btnInsert.Enabled = false;
            btnDelete.Enabled = false;
            btnUpdate.Enabled = false;

            btnSave.Enabled = true;
            btnCancel.Enabled = true;
        }

        bool isValidate()
        {
            if (txtName.Text == "" || txtEmail.Text == "" || txtPhone.Text == "" || txtAddress.Text == "" || 
                comboBox1.SelectedItem == null || numericUpDown1.Value == 0 || txtPassword.Text == "" || txtConfirmPassword.Text == "")
            {
                MessageBox.Show("Data tidak boleh kosong");
                return false;
            }

            if (txtPassword.Text != txtConfirmPassword.Text)
            {
                MessageBox.Show("Password dan Confirm Password tidak sama");
                return false;
            }
            return true;
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            actionClick();

            ID = 0;
            txtIdEmployee.Clear();
            txtName.Clear();
            txtEmail.Clear();
            txtPhone.Clear();
            txtAddress.Clear();
            dateTimePicker1.Value = DateTime.Now;
            comboBox1.SelectedItem = null;
            numericUpDown1.Value = 0;
            txtPassword.Clear();
            txtConfirmPassword.Clear();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                actionClick();
            }
            else
            {
                MessageBox.Show("Belum ada data yang dipilih");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                var res = MessageBox.Show("Yakin ingin menghapus data dengan Id " + ID, "Warning", MessageBoxButtons.OKCancel);

                if (res == DialogResult.OK)
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Delete from dbo.Employee Where id = '" + ID + "'", con);
                    com.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Data Berhasil Dihapus");

                    Refresh();
                }
            }
            else
            {
                MessageBox.Show("Belum ada data yang dipilih");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isValidate())
            {
                if (ID == 0)
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("INSERT into dbo.Employee(Password, Name, Email, Address, PhoneNumber, DateofBirth, IdJob, Salary) VALUES" +
                        "(@password, @name, @email, @phone, @address, @dob, @job, @salary) ", con);
                    com.Parameters.AddWithValue("password", txtPassword.Text);
                    com.Parameters.AddWithValue("name", txtName.Text);
                    com.Parameters.AddWithValue("email", txtEmail.Text);
                    com.Parameters.AddWithValue("phone", txtPhone.Text);
                    com.Parameters.AddWithValue("address", txtAddress.Text);
                    com.Parameters.AddWithValue("dob", dateTimePicker1.Value);
                    com.Parameters.AddWithValue("job", comboBox1.SelectedValue.ToString());
                    com.Parameters.AddWithValue("salary", numericUpDown1.Value.ToString());
                    com.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Data Berhasil Ditambahkan");
                }
                else
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("UPDATE dbo.Employee set Password = @password, Name = @name, Email = @email, PhoneNumber = @phone, DateOfBirth = @dob," +
                        " IdJob = @job, Salary = @salary Where id = '" + ID.ToString() + "' ", con);
                    com.Parameters.AddWithValue("password", txtPassword.Text);
                    com.Parameters.AddWithValue("name", txtName.Text);
                    com.Parameters.AddWithValue("email", txtEmail.Text);
                    com.Parameters.AddWithValue("phone", txtPhone.Text);
                    com.Parameters.AddWithValue("address", txtAddress.Text);
                    com.Parameters.AddWithValue("dob", dateTimePicker1.Value);
                    com.Parameters.AddWithValue("job", comboBox1.SelectedValue.ToString());
                    com.Parameters.AddWithValue("salary", numericUpDown1.Value.ToString());
                    com.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Data Berhasil DiEdit");
                }

                Refresh();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Refresh();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = 
                String.Format("Name like '%"+ txtSearch.Text +"%' or Email like '%"+ txtSearch.Text +"%' or PhoneNumber like '%"+ txtSearch.Text +"%'");
        }
    }
}
