﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LatihanH1.Controls
{
    public partial class TransactionDeposit : UserControl
    {
        SqlConnection con = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=Laundry;Integrated Security=True");
        List<Object> item = new List<Object>();
        public TransactionDeposit()
        {
            InitializeComponent();
        }

        private void TransactionDeposit_Load(object sender, EventArgs e)
        {
            LoadService();
            LoadAllRecord();
            Refresh();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AddCustomerForm form = new AddCustomerForm();
            form.ShowDialog();
        }
        void LoadService()
        {
            con.Open();
            string query = "Select * from dbo.Service";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
 
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "Id";
            con.Close();
        }
        void LoadAllRecord()
        {
            con.Open();
            string query = "Select * from dbo.Customer";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            comboBoxPhone.DataSource = dt;
            comboBoxPhone.DisplayMember = "PhoneNumber";
            comboBoxPhone.ValueMember = "Id";
            con.Close();
        }

        void Refresh()
        {
            comboBoxPhone.SelectedItem = null;
            comboBox1.SelectedItem = null;

        }


    }
}
