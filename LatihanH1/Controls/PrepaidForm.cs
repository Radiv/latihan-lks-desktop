﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LatihanH1.Controls
{
    public partial class PrepaidForm : UserControl
    {
        int IdCustomer = 0;
        public PrepaidForm()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=Laundry;Integrated Security=True");
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AddCustomerForm form = new AddCustomerForm();
            form.FormClosed += new FormClosedEventHandler(ChildFormClosed);
            form.Show();
        }

        void ChildFormClosed(object sender, FormClosedEventArgs e)
        {
            this.LoadAllCustomer();
        }

        private void PrepaidForm_Load(object sender, EventArgs e)
        {
            LoadAllPackage();
            LoadAllCustomer();
            Refresh();
        }
        void LoadAllPrepaidPackage()
        {
            con.Open();
            string query = "Select dbo.PrepaidPackage.Id AS Id, dbo.Customer.Name AS Customer, CONCAT(dbo.Service.Name, ' ', dbo.Package.TotalUnit, ' ', dbo.Unit.Name) AS Package, dbo.PrepaidPackage.Price AS Price  from dbo.PrepaidPackage " +
                "JOIN dbo.Package ON dbo.Package.Id = dbo.PrepaidPackage.IdPackage " +
                "JOIN dbo.Service ON dbo.Service.Id = dbo.Package.IdService " +
                "JOIN dbo.Unit ON dbo.Unit.Id = dbo.Service.IdUnit " +
                "JOIN dbo.Customer ON dbo.Customer.Id = dbo.PrepaidPackage.IdCustomer";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();
        }

        void LoadAllPackage()
        {
            con.Open();
            string query = "Select dbo.Package.Id AS Id, CONCAT(dbo.Service.Name, ' ', dbo.Package.TotalUnit, ' ', dbo.Unit.Name) AS Package from dbo.Package " +
                "JOIN dbo.Service ON dbo.Service.Id = dbo.Package.IdService " +
                "JOIN dbo.Unit ON dbo.Unit.Id = dbo.Service.IdUnit";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            comboPackage.DataSource = dt;
            comboPackage.DisplayMember = "Package";
            comboPackage.ValueMember = "Id";
            con.Close();
        }

        void LoadAllCustomer()
        {
            con.Open();
            string query = "Select * from dbo.Customer";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            comboCustomer.DataSource = dt;
            comboCustomer.DisplayMember = "PhoneNumber";
            comboCustomer.ValueMember = "Id";
            con.Close();
            comboCustomer.SelectedItem = null;
        }

        bool isValidate()
        {
            if (comboCustomer.SelectedItem == null || comboPackage.SelectedItem == null || numericPrice.Value == 0)
            {
                MessageBox.Show("Data tidak boleh kosong");
                return false;
            }

            return true;
        }

        void Refresh()
        {
            comboCustomer.SelectedItem = null;
            comboPackage.SelectedItem = null;
            numericPrice.Value = 0;
            LoadAllPrepaidPackage();
        }

        private void comboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            IdCustomer = (int)comboCustomer.SelectedValue;

            con.Open();
            string query = "Select Name, Address from dbo.Customer Where Id="+IdCustomer;
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows.Count == 1)
            {
                DataRow row = dt.Rows[0];
                labelNama.Text = row["Name"].ToString();
                labelAddress.Text = row["Address"].ToString();
            }

            con.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (isValidate())
            {
                con.Open();
                SqlCommand com = new SqlCommand("INSERT into dbo.PrepaidPackage(IdCustomer, IdPackage, Price, StartDatetime) VALUES" +
                    "(@customer, @package, @price, @date) ", con);
                com.Parameters.AddWithValue("customer", comboCustomer.SelectedValue);
                com.Parameters.AddWithValue("package", comboPackage.SelectedValue);
                com.Parameters.AddWithValue("price", numericPrice.Value);
                com.Parameters.AddWithValue("date", DateTime.Now.ToString("G"));
                com.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Data Berhasil Ditambahkan");

                Refresh();
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter =
                String.Format("Customer like '%" + txtSearch.Text + "%' or Package like '%" + txtSearch.Text + "%'");
        }
    }
}
