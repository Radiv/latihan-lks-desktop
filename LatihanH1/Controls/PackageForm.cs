﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace LatihanH1.Controls
{
    public partial class PackageForm : UserControl
    {
        SqlConnection con = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=Laundry;Integrated Security=True");
        int ID = 0;
        public PackageForm()
        {
            InitializeComponent();
        }

        private void PackageForm_Load(object sender, EventArgs e)
        {
            Refresh();
            LoadAllService();
        }
        void LoadAllRecord()
        {
            con.Open();
            string query = "SELECT * From dbo.Package";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();
        }

        void LoadAllService()
        {
            con.Open();
            string query = "Select * from dbo.Service";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            comboBoxService.DataSource = dt;
            comboBoxService.DisplayMember = "Name";
            comboBoxService.ValueMember = "Id";
            con.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.CurrentRow.Selected = true;
            int row = dataGridView1.CurrentCell.RowIndex;
            ID = (int)dataGridView1.Rows[row].Cells[0].Value;
            txtIdPackage.Text = dataGridView1.Rows[row].Cells[0].Value.ToString();
            comboBoxService.SelectedValue = dataGridView1.Rows[row].Cells[1].Value;
            numericUnit.Value = (int)dataGridView1.Rows[row].Cells[2].Value;
            numericPrice.Value = (int)dataGridView1.Rows[row].Cells[3].Value;
        }

        void Refresh()
        {
            dataGridView1.Enabled = true;
            ID = 0;

            txtIdPackage.Clear();
            comboBoxService.SelectedItem = null;
            numericUnit.Value = 0;
            numericPrice.Value = 0;

            txtIdPackage.Enabled = false;
            comboBoxService.Enabled = false;
            numericUnit.Enabled = false;
            numericPrice.Enabled = false;

            btnInsert.Enabled = true;
            btnDelete.Enabled = true;
            btnUpdate.Enabled = true;

            btnSave.Enabled = false;
            btnCancel.Enabled = false;

            LoadAllRecord();
        }

        void actionClick()
        {
            dataGridView1.Enabled = false;

            comboBoxService.Enabled = true;
            numericUnit.Enabled = true;
            numericPrice.Enabled = true;

            btnInsert.Enabled = false;
            btnDelete.Enabled = false;
            btnUpdate.Enabled = false;

            btnSave.Enabled = true;
            btnCancel.Enabled = true;
        }

        bool isValidate()
        {
            if (comboBoxService.SelectedItem == null || numericUnit.Value == 0 || numericPrice.Value == 0)
            {
                MessageBox.Show("Data tidak boleh kosong");
                return false;
            }

            return true;
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            actionClick();
            ID = 0;

            txtIdPackage.Clear();
            comboBoxService.SelectedItem = null;
            numericUnit.Value = 0;
            numericPrice.Value = 0;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                actionClick();
            }
            else
            {
                MessageBox.Show("Belum ada data yang dipilih");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                var res = MessageBox.Show("Yakin ingin menghapus data dengan Id " + ID, "Warning", MessageBoxButtons.OKCancel);

                if (res == DialogResult.OK)
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Delete from dbo.Package Where id = '" + ID + "'", con);
                    com.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Data Berhasil Dihapus");

                    Refresh();
                }
            }
            else
            {
                MessageBox.Show("Belum ada data yang dipilih");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isValidate())
            {
                if (ID == 0)
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("INSERT into dbo.Package(IdService, TotalUnit, Price) VALUES" +
                        "(@service, @unit, @price) ", con);
                    com.Parameters.AddWithValue("service", comboBoxService.SelectedValue);
                    com.Parameters.AddWithValue("unit", numericUnit.Value);
                    com.Parameters.AddWithValue("price", numericPrice.Value);
                    com.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Data Berhasil Ditambahkan");
                }
                else
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("UPDATE dbo.Package set IdService = @service, TotalUnit = @unit, " +
                        "Price = @price Where id = '" + ID.ToString() + "' ", con);
                    com.Parameters.AddWithValue("service", comboBoxService.SelectedValue);
                    com.Parameters.AddWithValue("unit", numericUnit.Value);
                    com.Parameters.AddWithValue("price", numericPrice.Value);
                    com.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Data Berhasil DiEdit");
                }

                Refresh();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Refresh();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter =
                String.Format("Name like '%" + txtSearch.Text + "%' or TotalUnit like '%" + txtSearch.Text + "%' " +
                "or Price like '%" + txtSearch.Text + "%'");
        }
    }
}
