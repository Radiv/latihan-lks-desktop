﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace LatihanH1.Controls
{
    public partial class ServiceForm : UserControl
    {
        SqlConnection con = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=Laundry;Integrated Security=True");
        int ID = 0;

        public ServiceForm()
        {
            InitializeComponent();
        }
        
        private void ServiceForm_Load(object sender, EventArgs e)
        {
            LoadAllRecord();
            LoadAllCategory();
            LoadAllUnit();
        }
        void LoadAllRecord()
        {
            con.Open();
            string query = "Select * from dbo.Service";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();
        }
        void LoadAllCategory()
        {
            con.Open();
            string query = "Select * from dbo.Category";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            comboBoxCategory.DataSource = dt;
            comboBoxCategory.DisplayMember = "Name";
            comboBoxCategory.ValueMember = "Id";
            con.Close();
        }  
        void LoadAllUnit()
        {
            con.Open();
            string query = "Select * from dbo.Unit";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            comboBoxUnit.DataSource = dt;
            comboBoxUnit.DisplayMember = "Name";
            comboBoxUnit.ValueMember = "Id";
            con.Close();
        }
        private void tampilData(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.CurrentRow.Selected = true;
            int row = dataGridView1.CurrentCell.RowIndex;
            ID = (int)dataGridView1.Rows[row].Cells[0].Value;
            txtIdService.Text = dataGridView1.Rows[row].Cells[0].Value.ToString();
            txtName.Text = dataGridView1.Rows[row].Cells[1].Value.ToString();
            comboBoxCategory.SelectedValue = dataGridView1.Rows[row].Cells[2].Value;
            comboBoxUnit.SelectedValue = dataGridView1.Rows[row].Cells[3].Value;
            numericPrice.Value = (int) dataGridView1.Rows[row].Cells[4].Value;
            numericDuration.Value = (int) dataGridView1.Rows[row].Cells[5].Value;
        }

        void Refresh()
        {
            dataGridView1.Enabled = true;
            ID = 0;

            txtIdService.Clear();
            txtName.Clear();
            comboBoxCategory.SelectedItem = null;
            comboBoxUnit.SelectedItem = null;
            numericPrice.Value = 0;
            numericDuration.Value = 0;

            txtIdService.Enabled = false;
            txtName.Enabled = false;
            comboBoxCategory.Enabled = false;
            comboBoxUnit.Enabled = false;
            numericPrice.Enabled = false;
            numericDuration.Enabled = false;

            btnInsert.Enabled = true;
            btnDelete.Enabled = true;
            btnUpdate.Enabled = true;

            btnSave.Enabled = false;
            btnCancel.Enabled = false;

            LoadAllRecord();
        }

        void actionClick()
        {
            dataGridView1.Enabled = false;

            txtName.Enabled = true;
            comboBoxCategory.Enabled = true;
            comboBoxUnit.Enabled = true;
            numericPrice.Enabled = true;
            numericDuration.Enabled = true;

            btnInsert.Enabled = false;
            btnDelete.Enabled = false;
            btnUpdate.Enabled = false;

            btnSave.Enabled = true;
            btnCancel.Enabled = true;
        }

        bool isValidate()
        {
            if (txtName.Text == "" || comboBoxUnit.SelectedItem == null ||
                comboBoxCategory.SelectedItem == null || numericPrice.Value == 0 || numericDuration.Value == 0)
            {
                MessageBox.Show("Data tidak boleh kosong");
                return false;
            }

            return true;
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            actionClick();
            ID = 0;

            txtIdService.Clear();
            txtName.Clear();
            comboBoxCategory.SelectedItem = null;
            comboBoxUnit.SelectedItem = null;
            numericPrice.Value = 0;
            numericDuration.Value = 0;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                actionClick();
            }
            else
            {
                MessageBox.Show("Belum ada data yang dipilih");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                var res = MessageBox.Show("Yakin ingin menghapus data dengan Id " + ID, "Warning", MessageBoxButtons.OKCancel);

                if (res == DialogResult.OK)
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Delete from dbo.Service Where id = '" + ID + "'", con);
                    com.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Data Berhasil Dihapus");

                    Refresh();
                }
            }
            else
            {
                MessageBox.Show("Belum ada data yang dipilih");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isValidate())
            {
                if (ID == 0)
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("INSERT into dbo.Service(Name, IdCategory, IdUnit, PriceUnit, EstimationDuration) VALUES" +
                        "(@name, @category, @unit, @price, @est) ", con);
                    com.Parameters.AddWithValue("name", txtName.Text);
                    com.Parameters.AddWithValue("category", comboBoxCategory.SelectedValue);
                    com.Parameters.AddWithValue("unit", comboBoxUnit.SelectedValue);
                    com.Parameters.AddWithValue("price", numericPrice.Value);
                    com.Parameters.AddWithValue("est", numericDuration.Value);
                    com.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Data Berhasil Ditambahkan");
                }
                else
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("UPDATE dbo.Service set Name = @name, IdCategory = @category, IdUnit = @unit, " +
                        "PriceUnit = @price, EstimationDuration = @est Where id = '" + ID.ToString() + "' ", con);
                    com.Parameters.AddWithValue("name", txtName.Text);
                    com.Parameters.AddWithValue("category", comboBoxCategory.SelectedValue);
                    com.Parameters.AddWithValue("unit", comboBoxUnit.SelectedValue);
                    com.Parameters.AddWithValue("price", numericPrice.Value);
                    com.Parameters.AddWithValue("est", numericDuration.Value);
                    com.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Data Berhasil DiEdit");
                }

                Refresh();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Refresh();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter =
                String.Format("Name like '%" + txtSearch.Text + "%' or IdCategory like '%" + txtSearch.Text + "%' " +
                "or IdUnit like '%" + txtSearch.Text + "%' or PriceUnit like '%" + txtSearch.Text + "%'");
        }

    }
}
